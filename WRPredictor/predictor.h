#ifndef PREDICTOR_H
#define PREDICTOR_H

#include <QMainWindow>
#include <subject.h>
#include <calculator.h>



namespace Ui {
class Predictor;
}

class Predictor : public QMainWindow
{
    Q_OBJECT

public:
    explicit Predictor(QWidget *parent = 0);
    ~Predictor();
    calculator calc;


private slots:
    void on_btnFemale_toggled(bool checked);

    void on_btnCalculate_clicked();

    void on_boxTraining_currentIndexChanged(int index);

    void on_btnMale_toggled(bool checked);

    void on_txtAge_textEdited(const QString &arg1);

    void on_txtHeight_textEdited(const QString &arg1);

    void on_txtWeight_textEdited(const QString &arg1);

    void on_txtHR_textEdited(const QString &arg1);

private:
    Ui::Predictor *ui;
    //QString bmi;

};

#endif // PREDICTOR_H

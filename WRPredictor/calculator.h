#ifndef CALCULATOR_H
#define CALCULATOR_H
#include <subject.h>
#include <QString>
#include <adresses.h>

class calculator
{
public:
    calculator();
    void calculate();
    //subject sbj;
    QString calcBMI();
    QString calcWR10();
    QString calcWR15();
};

#endif // CALCULATOR_H

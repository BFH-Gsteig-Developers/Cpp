#-------------------------------------------------
#
# Project created by QtCreator 2016-11-18T15:52:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WR-Predictor
TEMPLATE = app


SOURCES += main.cpp\
        predictor.cpp \
    calculator.cpp

HEADERS  += predictor.h \
    calculator.h \
    adresses.h

FORMS    += predictor.ui

#include "predictor.h"
#include "ui_predictor.h"

//#include <string>
#include <QString>




Predictor::Predictor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Predictor)
{
    ui->setupUi(this);



    QStringList list=(QStringList()<<"Inactive"<<"Regularly, low level exercise"<<"Aerobic exercise, moderate, 20-60 min/week"<<"Aerobic, 1-3 h/week"<<"Aerobic, over 3 h/week");
    ui->boxTraining->addItems(list);

}

Predictor::~Predictor()
{
    delete ui;
}

void Predictor::on_btnFemale_toggled(bool checked)
{
    ui->btnMale->setChecked(false);
    *sex=0;
}
void Predictor::on_btnMale_toggled(bool checked)
{
    ui->btnFemale->setChecked(false);
    *sex=2.77;
    ui->btnCalculate->setText(QString::number(*sex));
}

void Predictor::on_txtAge_textEdited(const QString &arg1)
{
    *age=arg1.toInt();
    //QString a= QString::number(sbj.getAge());
    //ui->txtBMI->setText(a);
}

void Predictor::on_txtHeight_textEdited(const QString &arg1)
{
    *height=arg1.toDouble();
    //QString a= QString::number(calc.sbj.getWeight()/(calc.sbj.getHeight()*calc.sbj.getHeight()));
    //ui->txtWR10->setText(a);
}

void Predictor::on_txtWeight_textEdited(const QString &arg1)
{
    *weight=arg1.toInt();
}

void Predictor::on_txtHR_textEdited(const QString &arg1)
{
    *hr=arg1.toInt();
}


void Predictor::on_boxTraining_currentIndexChanged(int index)
{
    switch(index)
    {
        case 0 :
            *factor=0;
        break;

        case 1 :
           *factor=0.32;
        break;

        case 2 :
            *factor=1.06;
        break;

        case 3 :
            *factor=1.76;
        break;

        case 4 :
            *factor=3.03;
        break;
    }
}

void Predictor::on_btnCalculate_clicked()
{
    //bmi=calc.calcBMI();
    ui->txtBMI->setText(calc.calcBMI());
    //ui->txtBMI->setText(QString::number(sbj.getWeight()/(sbj.getHeight()*sbj.getHeight()),'f', 2));


    ui->txtWR10->setText(calc.calcWR10());
    ui->txtWR15->setText(calc.calcWR15());
}

#include "stdafx.h"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>


using namespace cv;
using namespace std;

int main(int argc, const char** argv)
{
	Mat image;
	VideoCapture stream;
	stream.open("http://147.87.172.251:8081/?x.mjpeg");

	if (!stream.isOpened())
	{
		cout << "The connection failed" << endl;
		return -1;
	}

	while (true)
	{
		stream.read(image);
		imshow("Output Window", image);

		if (waitKey(1) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	

	

	waitKey(0); //wait infinite time for a keypress

	destroyWindow("MyWindow"); //destroy the window with the name, "MyWindow"

	return 0;
}